package br.com.apptenis.model;

import java.io.Serializable;
import java.util.Date;

public class Ranking implements Serializable {

	private static final long serialVersionUID = -4757337418707450563L;

	private Integer idRanking;
	private String nome;
	private String descricao;
	private Date dtInicio;
	private Date dtFim;
	private boolean ativo;
	private String tipo;

	public Integer getIdRanking() {
		return idRanking;
	}

	public void setIdRanking(Integer idRanking) {
		this.idRanking = idRanking;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getDtInicio() {
		return dtInicio;
	}

	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}

	public Date getDtFim() {
		return dtFim;
	}

	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
}
