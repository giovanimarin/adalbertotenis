package br.com.apptenis.model;

import java.io.Serializable;

public class TenistaRanking implements Serializable {

	private static final long serialVersionUID = -7211922946402906010L;
	private Integer idTenistaRanking;
	private Ranking ranking;
	private Tenista tenista;
	private Integer posicao;
	private Integer jogos;
	private Integer vitorias;
	private Integer derrotas;
	private Integer setsVencidos;
	private Integer setsPerdidos;
	private Integer gamesVencidos;
	private Integer gamesPerdidos;

	public TenistaRanking() {
		this.setRanking(new Ranking());
		this.setTenista(new Tenista());
	}

	public Integer getIdTenistaRanking() {
		return idTenistaRanking;
	}

	public void setIdTenistaRanking(Integer idTenistaRanking) {
		this.idTenistaRanking = idTenistaRanking;
	}

	public Ranking getRanking() {
		return ranking;
	}

	public void setRanking(Ranking ranking) {
		this.ranking = ranking;
	}

	public Tenista getTenista() {
		return tenista;
	}

	public void setTenista(Tenista tenista) {
		this.tenista = tenista;
	}

	public Integer getPosicao() {
		return posicao;
	}

	public void setPosicao(Integer posicao) {
		this.posicao = posicao;
	}

	public Integer getJogos() {
		return jogos;
	}

	public void setJogos(Integer jogos) {
		this.jogos = jogos;
	}

	public Integer getVitorias() {
		return vitorias;
	}

	public void setVitorias(Integer vitorias) {
		this.vitorias = vitorias;
	}

	public Integer getDerrotas() {
		return derrotas;
	}

	public void setDerrotas(Integer derrotas) {
		this.derrotas = derrotas;
	}

	public Integer getSetsVencidos() {
		return setsVencidos;
	}

	public void setSetsVencidos(Integer setsVencidos) {
		this.setsVencidos = setsVencidos;
	}

	public Integer getSetsPerdidos() {
		return setsPerdidos;
	}

	public void setSetsPerdidos(Integer setsPerdidos) {
		this.setsPerdidos = setsPerdidos;
	}

	public Integer getGamesVencidos() {
		return gamesVencidos;
	}

	public void setGamesVencidos(Integer gamesVencidos) {
		this.gamesVencidos = gamesVencidos;
	}

	public Integer getGamesPerdidos() {
		return gamesPerdidos;
	}

	public void setGamesPerdidos(Integer gamesPerdidos) {
		this.gamesPerdidos = gamesPerdidos;
	}
}
