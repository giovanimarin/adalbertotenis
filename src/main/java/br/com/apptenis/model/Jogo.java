package br.com.apptenis.model;

import java.io.Serializable;
import java.util.Date;

public class Jogo implements Serializable {

	private static final long serialVersionUID = 3777089800960950455L;
	private Integer idJogo;
	private Ranking ranking;
	private Quadra quadra;
	private Date data;

	public Jogo() {
		this.setRanking(new Ranking());
		this.setQuadra(new Quadra());
	}
	
	public Integer getIdJogo() {
		return idJogo;
	}

	public void setIdJogo(Integer idJogo) {
		this.idJogo = idJogo;
	}

	public Ranking getRanking() {
		return ranking;
	}

	public void setRanking(Ranking ranking) {
		this.ranking = ranking;
	}

	public Quadra getQuadra() {
		return quadra;
	}

	public void setQuadra(Quadra quadra) {
		this.quadra = quadra;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
}
