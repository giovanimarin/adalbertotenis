package br.com.apptenis.model;

import java.io.Serializable;

public class Quadra implements Serializable {

	private static final long serialVersionUID = 8593839637110415381L;
	private Integer idQuadra;
	private String nome;

	public Integer getIdQuadra() {
		return idQuadra;
	}

	public void setIdQuadra(Integer idQuadra) {
		this.idQuadra = idQuadra;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
