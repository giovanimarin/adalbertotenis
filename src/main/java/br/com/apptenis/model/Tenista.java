package br.com.apptenis.model;

import java.io.Serializable;
import java.util.Date;

public class Tenista implements Serializable {

	private static final long serialVersionUID = -2613680868017159858L;
	private Integer idTenista;
	private String nome;
	private String sobrenome;
	private Date dtNascimento;
	private byte[] foto;

	public Integer getIdTenista() {
		return idTenista;
	}
	
	public void setIdTenista(Integer idTenista) {
		this.idTenista = idTenista;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getSobrenome() {
		return sobrenome;
	}
	
	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}
	
	public Date getDtNascimento() {
		return dtNascimento;
	}
	
	public void setDtNascimento(Date dtNascimento) {
		this.dtNascimento = dtNascimento;
	}
	
	public byte[] getFoto() {
		return foto;
	}
	
	public void setFoto(byte[] foto) {
		this.foto = foto;
	}
}