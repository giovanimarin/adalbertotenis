package br.com.apptenis.model;

import java.io.Serializable;

public class Placar implements Serializable {

	private static final long serialVersionUID = 2036422721151588710L;
	private Integer idPlacar;
	private Jogo jogo;
	private Tenista tenista;
	private String resultado;
	private Integer setsVencidos;
	private Integer setsPerdidos;
	private Integer gamesVencidos;
	private Integer gamesPerdidos;

	public Placar() {
		this.setJogo(new Jogo());
		this.setTenista(new Tenista());
	}
	
	public Integer getIdPlacar() {
		return idPlacar;
	}

	public void setIdPlacar(Integer idPlacar) {
		this.idPlacar = idPlacar;
	}

	public Jogo getJogo() {
		return jogo;
	}

	public void setJogo(Jogo jogo) {
		this.jogo = jogo;
	}

	public Tenista getTenista() {
		return tenista;
	}

	public void setTenista(Tenista tenista) {
		this.tenista = tenista;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public Integer getSetsVencidos() {
		return setsVencidos;
	}

	public void setSetsVencidos(Integer setsVencidos) {
		this.setsVencidos = setsVencidos;
	}

	public Integer getSetsPerdidos() {
		return setsPerdidos;
	}

	public void setSetsPerdidos(Integer setsPerdidos) {
		this.setsPerdidos = setsPerdidos;
	}

	public Integer getGamesVencidos() {
		return gamesVencidos;
	}

	public void setGamesVencidos(Integer gamesVencidos) {
		this.gamesVencidos = gamesVencidos;
	}

	public Integer getGamesPerdidos() {
		return gamesPerdidos;
	}

	public void setGamesPerdidos(Integer gamesPerdidos) {
		this.gamesPerdidos = gamesPerdidos;
	}
}
